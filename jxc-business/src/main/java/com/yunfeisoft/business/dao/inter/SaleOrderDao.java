package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.SaleOrder;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleOrderDao
 * Description: 销售单信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SaleOrderDao extends BaseDao<SaleOrder, String> {

    public Page<SaleOrder> queryPage(Map<String, Object> params);

    public List<SaleOrder> queryByIds(String[] ids);

    public SaleOrder queryTotalAmount(String orgId, int status, int payStatus);
}