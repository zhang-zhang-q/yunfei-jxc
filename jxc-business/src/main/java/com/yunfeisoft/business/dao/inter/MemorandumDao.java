package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.Memorandum;

import java.util.Map;

/**
 * ClassName: MemorandumDao
 * Description: 备忘录信息Dao
 * Author: Jackie liu
 * Date: 2020-08-23
 */
public interface MemorandumDao extends BaseDao<Memorandum, String> {

    public Page<Memorandum> queryPage(Map<String, Object> params);
}