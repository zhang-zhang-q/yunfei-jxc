package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Constants;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.ProductCategoryDao;
import com.yunfeisoft.business.model.ProductCategory;
import com.yunfeisoft.business.service.inter.ProductCategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductCategoryServiceImpl
 * Description: 商品类别service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("productCategoryService")
public class ProductCategoryServiceImpl extends BaseServiceImpl<ProductCategory, String, ProductCategoryDao> implements ProductCategoryService {

    @Override
    @DataSourceChange(slave = true)
    public Page<ProductCategory> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<ProductCategory> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }

    @Override
    public List<ProductCategory> queryByName(String orgId, String name) {
        return getDao().queryByName(orgId, name);
    }

    @Override
    public int modify(ProductCategory productCategory) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("idPath", productCategory.getId());
        List<ProductCategory> childList = getDao().queryList(params);

        //设置idPath,codePath
        ProductCategory old = load(productCategory.getId());
        if (old.getName().equals(productCategory.getName())) {
            return getDao().update(productCategory);
        }

        String oldName = old.getNamePath();
        String newName = null;

        if (Constants.ROOT.equals(old.getParentId())) {
            newName = productCategory.getName();
            productCategory.setNamePath(newName);
        } else {
            newName = oldName.substring(0, oldName.lastIndexOf("/")) + "/" + productCategory.getName();
            productCategory.setNamePath(newName);
        }

        for (ProductCategory child : childList) {
            if (child.getId().equals(productCategory.getId())) {
                continue;
            }

            child.setNamePath(child.getNamePath().replaceFirst(oldName, newName));
            getDao().update(child);
        }

        return getDao().update(productCategory);
    }

    @Override
    public int modifyWithDrag(String id, String targetId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("idPath", id);
        List<ProductCategory> childList = getDao().queryList(params);

        //设置idPath,codePath
        ProductCategory old = load(id);
        ProductCategory target = null;
        if (Constants.ROOT.equals(targetId)) {
            target = new ProductCategory();
            target.setNamePath("");
            target.setName("");
            target.setIdPath("");
            target.setId(Constants.ROOT);
        } else {
            target = load(targetId);
        }

        String oldPath = old.getIdPath();
        String oldName = old.getNamePath();

        if (StringUtils.isBlank(target.getIdPath())) {
            old.setIdPath(id);
        } else {
            old.setIdPath(target.getIdPath() + "/" + id);
        }

        old.setParentId(targetId);
        if (StringUtils.isBlank(target.getNamePath())) {
            old.setNamePath(old.getName());
        } else {
            old.setNamePath(target.getNamePath() + "/" + old.getName());
        }

        String newPath = old.getIdPath();
        String newCode = old.getNamePath();

        for (ProductCategory child : childList) {
            if (child.getId().equals(id)) {
                continue;
            }

            child.setIdPath(child.getIdPath().replaceFirst(oldPath, newPath));
            child.setNamePath(child.getNamePath().replaceFirst(oldName, newCode));
            getDao().update(child);
        }

        return getDao().update(old);
    }
}