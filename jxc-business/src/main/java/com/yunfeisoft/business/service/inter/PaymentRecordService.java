package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.PaymentRecord;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PaymentRecordService
 * Description: 付款信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface PaymentRecordService extends BaseService<PaymentRecord, String> {

    public Page<PaymentRecord> queryPage(Map<String, Object> params);

    public List<PaymentRecord> queryByPurchaseOrderId(String purchaseOrderId);

    public int batchSave2(String orgId, String[] purchaseOrderIds);
}