package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.WarehouseUserDao;
import com.yunfeisoft.business.model.WarehouseUser;
import com.yunfeisoft.business.service.inter.WarehouseUserService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: WarehouseUserServiceImpl
 * Description: 仓库用户信息service实现
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Service("warehouseUserService")
public class WarehouseUserServiceImpl extends BaseServiceImpl<WarehouseUser, String, WarehouseUserDao> implements WarehouseUserService {

    @Override
    @DataSourceChange(slave = true)
    public Page<WarehouseUser> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }
}